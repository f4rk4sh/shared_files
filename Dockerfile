FROM python:3.10.10-slim-bullseye

WORKDIR /src
COPY . /src

RUN python -m pip install --no-cache-dir virtualenv==20.17.1
